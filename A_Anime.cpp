/*[][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
  []                                                                      []
  []   Algorithm given by Dr. Rafa Arce Nazario for the discussion of the []
  [] problem #1 of the programming competition in October 5, 2013.        []
  []   Implementation made by Ramon Luis Collazo Martis, a student of the []
  [] UPRRP.                                                               []
  []                                                                      []
  [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]*/

#include<iostream>
#include<stack>
#include<queue>
using namespace std;

void printLine();
void useStack(stack<int>);

int main(){
   printLine();
   printLine();
   cout << "  Enter the number of cases and for each case the number of\n"
        << "chapters of Naruto, how many have been seen, how many has\n"
        << "recorded and how many filler they are. Also, after that,the\n"
        << "number of the episodes that are recorded followed by those\n"
        << "that are fillers.\n";
   printLine();
   
   int cases, N, P, J, F, tmp;
   char *chapters;
   stack<int> nanswers, eanswers;
   queue< stack<int> > sols;

   cin >> cases;
   for (int i = 0; i < cases; i++){
      cin >> N;
      chapters = new char[N];
      
      cin >> P;
      for (int j = 0; j < P; j++)
         chapters[j] = 'w';
      
      cin >> J >> F;
      for (int j = 0; j < J; j++){
         cin >> tmp;
         if (chapters[tmp] == 'w')
            chapters[j] = 'e';
         else chapters[j] = 'r';
      }
      for (int j = 0; j < F; j++){
         cin >> tmp;
         if (chapters[tmp] == 'n')
            chapters[j] = 'f';
         else if (chapters[tmp] == 'r')
            chapters[j] = 'e';
         else;
      }
      
      for (int j = N-1; j >= 0; j--){
         if (chapters[j] == 'n'){
            nctr++;
            nanswers.push(j+1);
         }
         else if (chapters[j] == 'e'){
            ectr++;
            eanswers.push(j+1);
         }
         else;
      }
      nanswers.push(nctr);
      eanswers.push(ectr);
      sols.push(eanswers);
      sols.push(nanswers);

      while (!nanswers.empty()) nanswers.pop();
      while (!eanswers.empty()) eanswers.pop();
      delete []chapters;
   }

   while (!sols.empty()){
      useStack(sols.front);
      cout << endl;
      sols.pop();
   }

   printLine();
   printLine();

   return 0;
}

void printLine(){
   cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n";
}

void useStack(stack<int> answer){
   while (!answer.empty()){
      cout << answer.top() << " ";
      answer.pop();
   }
}
