A: A_Anime.o
	g++ -o A A_Anime.o

A_Anime.o: A_Anime.cpp
	g++ -c A_Anime.cpp

clean:
	rm *.o .*.swp A
